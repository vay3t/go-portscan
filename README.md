# go-portscan

Golang Port Scanner standalone

## Build on Linux

### For Linux

```bash
git clone https://gitlab.com/vay3t/go-portscan
cd go-portscan
GOOS=linux GOARCH=amd64 go build -ldflags "-s -w" -trimpath go-portscan.go
upx go-portscan
```

### For Windows

```bash
git clone https://gitlab.com/vay3t/go-portscan
cd go-portscan
GOOS=windows GOARCH=amd64 go build -ldflags "-s -w" -trimpath go-portscan.go
upx go-portscan.exe
```

### For MacOS

```bash
git clone https://gitlab.com/vay3t/go-portscan
cd go-portscan
GOOS=darwin GOARCH=amd64 go build -ldflags "-s -w" -trimpath go-portscan.go
upx go-portscan
```

### For Docker

```bash
git clone https://gitlab.com/vay3t/go-portscan
cd go-portscan
docker build -t go-portscan .
```

## Usage

```bash
Usage of ./go-portscan:
  -delay float
        Delay in seconds
  -host string
        Host to scan: Ex, 192.168.0.0/24 or 192.168.0.1/32
  -ports string
        Ports to scan: Ex, 80,443,1-65535,1000-2000, ... (default "1-1024")
  -threads int
        Number of threads (default 5)
  -timeout float
        Timeout in seconds (default 5)
  -verbose
        Verbose
```

### Docker

```bash
docker run -ti --rm go-portscan -host TARGET/MASK -ports 21,22,80
```
