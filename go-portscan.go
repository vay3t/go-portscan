package main

import (
	"bytes"
	"encoding/hex"
	"flag"
	"fmt"
	"log"
	"math"
	"math/big"
	"net"
	"os"
	"sort"
	"strconv"
	"strings"
	"sync"
	"time"
)

var (
	host, ports    string
	threads        int
	timeout, delay float64
	verbose        bool
	addr_list      []string
)

func main() {

	flag.Float64Var(&timeout, "timeout", 5, "Timeout in seconds")
	flag.Float64Var(&delay, "delay", 0, "Delay in seconds")

	flag.StringVar(&host, "host", "", "Host to scan: Ex, 192.168.0.0/24 or 192.168.0.1/32")
	flag.StringVar(&ports, "ports", "1-1024", "Ports to scan: Ex, 80,443,1-65535,1000-2000, ...")

	flag.IntVar(&threads, "threads", 5, "Number of threads")

	flag.BoolVar(&verbose, "verbose", false, "Verbose")

	flag.Parse()

	listPorts := parsePorts()

	/*
		addr := fmt.Sprintf("%s:%d", target, port)
	*/

	if host != "" {
		c, _ := ParseCIDR(host)
		if err := c.ForEachIP(func(ip string) error {
			for _, port := range listPorts {
				addr := fmt.Sprintf("%s:%d", ip, port)
				addr_list = append(addr_list, addr)
			}
			return nil
		}); err != nil {
			fmt.Println(err)
		}

		if verbose {
			if host != "" {
				fmt.Println("+ Target: " + host)
			}

			fmt.Println("Hosts: " + strconv.Itoa(len(addr_list)))
			fmt.Println("Count(*) Ports: " + fmt.Sprint(len(listPorts)))
			fmt.Println("Threads: " + fmt.Sprint(threads))
			fmt.Println("Timeout: " + fmt.Sprint(timeout) + " Seconds")
			fmt.Println("Delay: " + fmt.Sprint(delay) + " Seconds")
			fmt.Println("===========================================")
		}

		var batch []string

		hitCounter := 0
		for _, address := range addr_list {
			batch = append(batch, address)
			hitCounter++
			if hitCounter < threads {
				continue
			}

			scanPorts(batch)

			batch = make([]string, 0)
			hitCounter = 0
		}

		if hitCounter != 0 {
			scanPorts(batch)
		}

	} else {
		fmt.Println("Usage:")
		flag.PrintDefaults()
		os.Exit(1)
	}

}

func scanTCP(addr string) string {
	// ip:puerto
	conn, err := net.DialTimeout("tcp", addr, time.Duration(timeout)*time.Second)

	if err != nil {
		return ""
	}

	conn.Close()

	return fmt.Sprintf("[+] %s Port %s open\n", strings.Split(addr, ":")[:1], strings.Split(addr, ":")[1:2])
}

func parsePorts() []int {
	var listPorts []int

	blocks := strings.Split(ports, ",")
	for _, block := range blocks {
		rg := strings.Split(block, "-")
		var minPort, maxPort int
		var err error

		minPort, err = strconv.Atoi(rg[0])
		if err != nil {
			log.Print("No ha sido posible interpretar el rango: ", block)
			continue
		}

		if len(rg) == 1 {
			maxPort = minPort
		} else {
			maxPort, err = strconv.Atoi(rg[1])
			if err != nil {
				log.Print("No ha sido posible interpretar el rango: ", block)
				continue
			}
		}

		for port := minPort; port <= maxPort; port++ {
			listPorts = append(listPorts, port)
		}

	}

	return listPorts
}

func scanPorts(addr_list []string) {
	var wg sync.WaitGroup
	for i := 0; i < len(addr_list); i++ {
		wg.Add(1)
		go func(addr string) {
			defer wg.Done()
			if verbose {
				log.Printf("Scanning %s port %s\n", strings.Split(addr, ":")[:1], strings.Split(addr, ":")[1:2])
			}
			fmt.Print(scanTCP(addr))
			if delay != 0 {
				time.Sleep(time.Duration(delay) * time.Second)
			}
		}(addr_list[i])
	}
	wg.Wait()
}

// https://github.com/3th1nk/cidr/

/*
	https://en.wikipedia.org/wiki/Classless_Inter-Domain_Routing
	CIDR表示法:
	IPv4   	网络号/前缀长度		192.168.1.0/24
	IPv6	接口号/前缀长度		2001:db8::/64
*/

type CIDR struct {
	ip    net.IP
	ipnet *net.IPNet
}

// 解析CIDR网段
func ParseCIDR(s string) (*CIDR, error) {
	i, n, err := net.ParseCIDR(s)
	if err != nil {
		return nil, err
	}
	return &CIDR{ip: i, ipnet: n}, nil
}

// 判断网段是否相等
func (c CIDR) Equal(ns string) bool {
	c2, err := ParseCIDR(ns)
	if err != nil {
		return false
	}
	return c.ipnet.IP.Equal(c2.ipnet.IP) /* && c.ipnet.IP.Equal(c2.ip) */
}

// 判断是否IPv4
func (c CIDR) IsIPv4() bool {
	_, bits := c.ipnet.Mask.Size()
	return bits/8 == net.IPv4len
}

// 判断是否IPv6
func (c CIDR) IsIPv6() bool {
	_, bits := c.ipnet.Mask.Size()
	return bits/8 == net.IPv6len
}

// 判断IP是否包含在网段中
func (c CIDR) Contains(ip string) bool {
	return c.ipnet.Contains(net.ParseIP(ip))
}

// 根据子网掩码长度校准后的CIDR
func (c CIDR) CIDR() string {
	return c.ipnet.String()
}

// CIDR字符串中的IP部分
func (c CIDR) IP() string {
	return c.ip.String()
}

// 网络号
func (c CIDR) Network() string {
	return c.ipnet.IP.String()
}

// 子网掩码位数
func (c CIDR) MaskSize() (ones, bits int) {
	ones, bits = c.ipnet.Mask.Size()
	return
}

// 子网掩码
func (c CIDR) Mask() string {
	mask, _ := hex.DecodeString(c.ipnet.Mask.String())
	return net.IP([]byte(mask)).String()
}

// [Deprecated] 网关地址，无法计算出来，可以是网段中任意IP地址
func (c CIDR) Gateway() string {
	return ""
}

// 广播地址(网段最后一个IP)
func (c CIDR) Broadcast() string {
	mask := c.ipnet.Mask
	bcst := make(net.IP, len(c.ipnet.IP))
	copy(bcst, c.ipnet.IP)
	for i := 0; i < len(mask); i++ {
		ipIdx := len(bcst) - i - 1
		bcst[ipIdx] = c.ipnet.IP[ipIdx] | ^mask[len(mask)-i-1]
	}
	return bcst.String()
}

// 起始IP、结束IP
func (c CIDR) IPRange() (start, end string) {
	return c.Network(), c.Broadcast()
}

// IP数量
func (c CIDR) IPCount() *big.Int {
	ones, bits := c.ipnet.Mask.Size()
	return big.NewInt(0).Lsh(big.NewInt(1), uint(bits-ones))
}

// 遍历网段下所有IP
func (c CIDR) ForEachIP(iterator func(ip string) error) error {
	next := make(net.IP, len(c.ipnet.IP))
	copy(next, c.ipnet.IP)
	for c.ipnet.Contains(next) {
		if err := iterator(next.String()); err != nil {
			return err
		}
		IncrIP(next)
	}
	return nil
}

// 从指定IP开始遍历网段下后续的IP
func (c CIDR) ForEachIPBeginWith(beginIP string, iterator func(ip string) error) error {
	next := net.ParseIP(beginIP)
	for c.ipnet.Contains(next) {
		if err := iterator(next.String()); err != nil {
			return err
		}
		IncrIP(next)
	}
	return nil
}

// 裂解子网的方式
const (
	SUBNETTING_METHOD_SUBNET_NUM = 0 // 基于子网数量
	SUBNETTING_METHOD_HOST_NUM   = 1 // 基于主机数量
)

// 裂解网段
func (c CIDR) SubNetting(method, num int) ([]*CIDR, error) {
	if num < 1 || (num&(num-1)) != 0 {
		return nil, fmt.Errorf("裂解数量必须是2的次方")
	}

	newOnes := int(math.Log2(float64(num)))
	ones, bits := c.MaskSize()
	switch method {
	default:
		return nil, fmt.Errorf("不支持的裂解方式")
	case SUBNETTING_METHOD_SUBNET_NUM:
		newOnes = ones + newOnes
		// 如果子网的掩码长度大于父网段的长度，则无法裂解
		if newOnes > bits {
			return nil, nil
		}
	case SUBNETTING_METHOD_HOST_NUM:
		newOnes = bits - newOnes
		// 如果子网的掩码长度小于等于父网段的掩码长度，则无法裂解
		if newOnes <= ones {
			return nil, nil
		}
		// 主机数量转换为子网数量
		num = int(math.Pow(float64(2), float64(newOnes-ones)))
	}

	cidrs := []*CIDR{}
	network := make(net.IP, len(c.ipnet.IP))
	copy(network, c.ipnet.IP)
	for i := 0; i < num; i++ {
		cidr, _ := ParseCIDR(fmt.Sprintf("%v/%v", network.String(), newOnes))
		cidrs = append(cidrs, cidr)

		// 广播地址的下一个IP即为下一段的网络号
		network = net.ParseIP(cidr.Broadcast())
		IncrIP(network)
	}

	return cidrs, nil
}

// 合并网段
func SuperNetting(ns []string) (*CIDR, error) {
	num := len(ns)
	if num < 1 || (num&(num-1)) != 0 {
		return nil, fmt.Errorf("子网数量必须是2的次方")
	}

	mask := ""
	cidrs := []*CIDR{}
	for _, n := range ns {
		// 检查子网CIDR有效性
		c, err := ParseCIDR(n)
		if err != nil {
			return nil, fmt.Errorf("网段%v格式错误", n)
		}
		cidrs = append(cidrs, c)

		// TODO 暂只考虑相同子网掩码的网段合并
		if len(mask) == 0 {
			mask = c.Mask()
		} else if c.Mask() != mask {
			return nil, fmt.Errorf("子网掩码不一致")
		}
	}
	AscSortCIDRs(cidrs)

	// 检查网段是否连续
	var network net.IP
	for _, c := range cidrs {
		if len(network) > 0 {
			if !network.Equal(c.ipnet.IP) {
				return nil, fmt.Errorf("必须是连续的网段")
			}
		}
		network = net.ParseIP(c.Broadcast())
		IncrIP(network)
	}

	// 子网掩码左移，得到共同的父网段
	c := cidrs[0]
	ones, bits := c.MaskSize()
	ones = ones - int(math.Log2(float64(num)))
	c.ipnet.Mask = net.CIDRMask(ones, bits)
	c.ipnet.IP.Mask(c.ipnet.Mask)

	return c, nil
}

// IP地址自增
func IncrIP(ip net.IP) {
	for i := len(ip) - 1; i >= 0; i-- {
		ip[i]++
		if ip[i] > 0 {
			break
		}
	}
}

// IP地址自减
func DecrIP(ip net.IP) {
	length := len(ip)
	for i := length - 1; i >= 0; i-- {
		ip[length-1]--
		if ip[length-1] < 0xFF {
			break
		}
		for j := 1; j < length; j++ {
			ip[length-j-1]--
			if ip[length-j-1] < 0xFF {
				return
			}
		}
	}
}

// 比较IP大小
// a等于b，返回0; a大于b，返回+1; a小于b，返回-1
func Compare(a, b net.IP) int {
	return bytes.Compare(a, b)
}

// 升序
func AscSortCIDRs(cs []*CIDR) {
	sort.Slice(cs, func(i, j int) bool {
		if n := bytes.Compare(cs[i].ipnet.IP, cs[j].ipnet.IP); n != 0 {
			return n < 0
		}

		if n := bytes.Compare(cs[i].ipnet.Mask, cs[j].ipnet.Mask); n != 0 {
			return n < 0
		}

		return false
	})
}

// 降序
func DescSortCIDRs(cs []*CIDR) {
	sort.Slice(cs, func(i, j int) bool {
		if n := bytes.Compare(cs[i].ipnet.IP, cs[j].ipnet.IP); n != 0 {
			return n >= 0
		}

		if n := bytes.Compare(cs[i].ipnet.Mask, cs[j].ipnet.Mask); n != 0 {
			return n >= 0
		}

		return false
	})
}
